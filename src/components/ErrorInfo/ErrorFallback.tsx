import errorIcon from "img/n1-error.webp";
import { FallbackProps } from "react-error-boundary";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { useNavigate } from "react-router-dom";
import { Button } from "@mui/material";
import "./ErrorFallback.scss";

export function ErrorFallback({ error, resetErrorBoundary }: FallbackProps) {
  const navigate = useNavigate();

  const resetError = () => {
    resetErrorBoundary();
    return navigate(-1);
  };

  return (
    <div className="container error-info">
      <div className="error-info__content">
        <div className="error-info__logo">
          <LazyLoadImage
            src={errorIcon}
            alt="error info"
            width="135"
            height="130"
          />
        </div>
        <div className="error-info__title">{error.message}</div>
      </div>
      <Button variant="contained" onClick={resetError}>
        Хорошо
      </Button>
    </div>
  );
}
