import * as React from "react";
import Button from "@mui/material/Button";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import FitlerMenu from "./FitlerMenu";

type Props = {
  label: string;
};

export default function Filter(props: Props) {
  const { label } = props;
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <Button
        variant="contained"
        disableElevation
        endIcon={<KeyboardArrowDownIcon />}
        color="inherit"
        sx={{
          fontSize: "14px",
          fontWeight: 700,
          color: "#333333",
          height: "40px",
        }}
        onClick={handleClick}
      >
        {label}
      </Button>
      <FitlerMenu anchorEl={anchorEl} handleClose={handleClose} />
    </>
  );
}
