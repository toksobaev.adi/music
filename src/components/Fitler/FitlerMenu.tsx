import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import { Box, Checkbox, FormControlLabel, FormGroup } from "@mui/material";
import Search from "components/Search/Search";
import { useState } from "react";

type Props = {
  anchorEl: HTMLElement | null;
  handleClose: () => void;
};

export default function FitlerMenu({ anchorEl, handleClose }: Props) {
  const open = Boolean(anchorEl);
  const [value, setValue] = useState("");

  return (
    <Menu anchorEl={anchorEl} open={open} onClose={handleClose}>
      <Box pl={2} pt={1} pb={1} pr={2}>
        <Search value={value} onChange={setValue} placeholder="Найти" />
      </Box>
      <FormGroup>
        <MenuItem>
          <FormControlLabel control={<Checkbox />} label="Кыргызская эстрада" />
        </MenuItem>
        <MenuItem>
          <FormControlLabel control={<Checkbox />} label="Кыргызский рэп" />
        </MenuItem>
        <MenuItem>
          <FormControlLabel control={<Checkbox />} label="Русский рок" />
        </MenuItem>
      </FormGroup>
    </Menu>
  );
}
