import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { LazyLoadImage } from "react-lazy-load-image-component";
import logo from "img/logo-white.png";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";

export default function Header() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar
        position="static"
        sx={{ boxShadow: "0px 2px 15px 0px #0000001A" }}
      >
        <Toolbar className="container">
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            <LazyLoadImage src={logo} alt="Logo" width="122" height="24" />
          </Typography>
          <Button color="inherit" startIcon={<AccountCircleIcon />}>
            Выйти
          </Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
