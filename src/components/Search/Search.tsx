import { InputAdornment, TextField } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";

type Props = {
  value: string;
  onChange: (value: string) => void;
  placeholder: string;
};

export default function Search(props: Props) {
  const { value, onChange, placeholder } = props;

  return (
    <TextField
      variant="outlined"
      value={value}
      onChange={(event) => onChange(event.target.value)}
      placeholder={placeholder}
      fullWidth
      sx={{
        ".MuiInputBase-root": {
          height: "40px",
          background:
            "linear-gradient(0deg, #F4F4F4, #F4F4F4),linear-gradient(0deg, #E7E6F2, #E7E6F2)",
        },
        ".MuiOutlinedInput-notchedOutline": {
          borderColor: " #E7E6F2",
        },
      }}
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <SearchIcon />
          </InputAdornment>
        ),
      }}
    />
  );
}
