import React from "react";
import AddPhotoAlternateOutlinedIcon from "@mui/icons-material/AddPhotoAlternateOutlined";
import "./Upload.scss";

const PhotoUploader = (props: any) => {
  const hiddenFileInput = React.useRef<any>(null);

  const handleClick = () => {
    hiddenFileInput.current.click();
  };

  const handleChange = (event: any) => {
    const fileUploaded = event.target.files[0];
    props.handleFile(fileUploaded);
  };

  return (
    <>
      <div className="photo-upload" onClick={handleClick}>
        <div className="photo-upload__content">
          <AddPhotoAlternateOutlinedIcon />
          <span className="photo-upload__label">Добавить изображение</span>
        </div>
      </div>
      <input
        type="file"
        accept="image/*"
        ref={hiddenFileInput}
        onChange={handleChange}
        style={{ display: "none" }}
      />
    </>
  );
};
export default PhotoUploader;
