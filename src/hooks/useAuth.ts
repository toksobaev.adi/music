import { useSelectorHook } from "./redux";


export function useAuth() {
  const { sessionId } = useSelectorHook((state) => state.app);

  return {
    isAuth: !!sessionId
  }
}