import React from "react";
import ReactDOM from "react-dom/client";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { ErrorBoundary } from "react-error-boundary";
import { store } from "./store";
import MainRouter from "./router";
import { ErrorFallback } from "components/ErrorInfo";
import { ToastContainer } from "react-toastify";
import { ThemeProvider } from "@mui/material/styles";
import mainTheme from "utils/theme";
import "react-toastify/dist/ReactToastify.css";
import "./assets/styles/index.scss";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <BrowserRouter basename={import.meta.env.VITE_PUBLIC_URL}>
      <ToastContainer
        autoClose={5000}
        hideProgressBar={false}
        closeOnClick={true}
        pauseOnHover={true}
        draggable={true}
        theme="colored"
      />
      <Provider store={store}>
        <ErrorBoundary FallbackComponent={ErrorFallback}>
          <ThemeProvider theme={mainTheme}>
            <MainRouter />
          </ThemeProvider>
        </ErrorBoundary>
      </Provider>
    </BrowserRouter>
  </React.StrictMode>
);
