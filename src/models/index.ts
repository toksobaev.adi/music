export type LoginDto = {
  login: string;
  password: string;
};

export type MusicParams = {
  pageIndex?: number;
  pageSize?: number;
  categoryId?: string;
  search?: string;
};

export type MusicDto = {
  musicId: string;
  groupName: string;
  trackName: string;
  imageBase64: string;
  categoryIds: any[];
};

export type CategoriesResponse = {
  result: Category[];
  ok: boolean;
  error: ErrorResponse;
};

export interface Category {
  categoryId: string;
  name: string;
  iconBase64: string;
  sequenceNumber: number;
}

export type ErrorResponse = {
  errorMessage?: string;
  errorCode?: string;
  additionalInfo?: string;
};
