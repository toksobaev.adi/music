import { Box, TextField } from "@mui/material";
import logo from "img/logo.png";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { LoadingButton } from "@mui/lab";
import { useForm, SubmitHandler } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { LoginDto } from "models/index";
import { Loader } from "components/Loader";
import "./Authorization.scss";

const schema = yup.object({
  login: yup.string().required("логин - обязательное поле"),
  password: yup
    .string()
    .required("пароль - обязательное поле")
    .min(6, "длина должна быть не менее 6 символов"),
});

export default function Authorization() {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<LoginDto>({
    resolver: yupResolver(schema),
  });

  const onSubmit: SubmitHandler<LoginDto> = async (data) => {
    setLoading(true);
    console.log("data", data);
    if (data) {
      return navigate("/", { replace: true });
    }
    setLoading(false);
  };

  if (loading) {
    return <Loader />;
  }

  return (
    <Box className="authorization">
      <div className="authorization__content">
        <img src={logo} alt="" className="authorization__logo" />
        <form className="authorization__row" onSubmit={handleSubmit(onSubmit)}>
          <div className="title">Вход</div>
          <TextField
            {...register("login")}
            variant="outlined"
            label="Логин"
            fullWidth
            className="authorization__input"
            error={Boolean(errors.login)}
            helperText={errors.login?.message}
          />
          <TextField
            {...register("password")}
            variant="outlined"
            type="password"
            label="Пароль"
            fullWidth
            className="authorization__input"
            error={Boolean(errors.password)}
            helperText={errors.password?.message}
          />
          <LoadingButton
            type="submit"
            variant="contained"
            fullWidth
            loading={loading}
          >
            Далее
          </LoadingButton>
        </form>
      </div>
    </Box>
  );
}
