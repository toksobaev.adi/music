import { Box, Button, Stack } from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";
import MelodyTable from "components/MelodyTable/MelodyTable";
import Filter from "components/Fitler/Filter";
import "./Home.scss";
import Search from "components/Search/Search";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

function Home() {
  const navigate = useNavigate();
  const [seachText, setSearch] = useState("");

  return (
    <Box className="container">
      <Grid container spacing={2} mt={2} justifyContent="space-between">
        <Grid xs={10}>
          <Box className="title">Мелодии</Box>
        </Grid>
        <Grid xs={2}>
          <Button
            variant="contained"
            fullWidth
            onClick={() => navigate("/set-music")}
          >
            Загрузить мелодию
          </Button>
        </Grid>
        <Grid xs={9}>
          <Stack spacing={2} direction="row">
            <Filter label="Жанр" />
            <Filter label="Исполнитель" />
            <Filter label="Дата загрузки" />
            <Filter label="Установки" />
          </Stack>
        </Grid>
        <Grid xs={3}>
          <Search
            value={seachText}
            onChange={setSearch}
            placeholder="Найти мелодию"
          />
        </Grid>
      </Grid>
      <MelodyTable />
    </Box>
  );
}

export default Home;
