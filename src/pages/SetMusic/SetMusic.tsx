import {
  Autocomplete,
  Box,
  Chip,
  CircularProgress,
  Stack,
  TextField,
} from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";
import PhotoUploader from "components/Upload/PhotoUploader";
import { useForm, SubmitHandler } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { Category, MusicDto } from "models/index";
import { useState } from "react";
import { useGetCategoriesQuery } from "store/categories/categories.api";
import { LoadingButton } from "@mui/lab";

const schema = yup.object({
  musicId: yup.string(),
  trackName: yup.string().required("Название - обязательное поле"),
  groupName: yup.string().required("Исполнитель - обязательное поле"),
  imageBase64: yup.string().required("Изображение - обязательное поле"),
  categoryIds: yup.array().required("Жанр - обязательное поле"),
});

function SetMusic() {
  const { data, isLoading: loading } = useGetCategoriesQuery("");
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<MusicDto>({
    resolver: yupResolver(schema as any),
  });

  const [selectRules, setSelectRules] = useState<Category[]>([]);

  const onSubmit: SubmitHandler<MusicDto> = async (data) => {
    console.log("data", data);
  };

  return (
    <Box
      component="form"
      className="container"
      onSubmit={handleSubmit(onSubmit)}
      pb={5}
    >
      <Grid container spacing={2} mt={2} justifyContent="center">
        <Grid xs={7}>
          <Box className="title">Загрузка мелодии</Box>
          <Box sx={{ mb: 5 }}>
            <Box sx={{ fontSize: "20px", fontWeight: 700, mb: 2 }}>Обложка</Box>
            <PhotoUploader />
          </Box>
          <Box sx={{ mb: 5 }}>
            <Box sx={{ fontSize: "20px", fontWeight: 700, mb: 2 }}>
              Описание
            </Box>
            <Stack spacing={3} direction="column">
              <TextField
                {...register("trackName")}
                variant="outlined"
                label="Название"
                fullWidth
                error={Boolean(errors.trackName)}
                helperText={errors.trackName?.message}
              />
              <TextField
                {...register("groupName")}
                variant="outlined"
                label="Исполнитель"
                fullWidth
                error={Boolean(errors.groupName)}
                helperText={errors.groupName?.message}
              />

              <Autocomplete
                multiple
                value={selectRules}
                size="small"
                onChange={(_, newValue) => {
                  setSelectRules(newValue);
                }}
                options={data ? data.result : []}
                loading={loading}
                disabled={loading}
                getOptionLabel={(option) => option.name || ""}
                renderTags={(tagValue, getTagProps) =>
                  tagValue.map((option, index) => (
                    <Chip
                      label={option.name}
                      size="small"
                      {...getTagProps({ index })}
                    />
                  ))
                }
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant="outlined"
                    label="Ограничение по условиям работы"
                    size="small"
                    InputProps={{
                      ...params.InputProps,
                      endAdornment: (
                        <>
                          {loading ? (
                            <CircularProgress color="inherit" size={16} />
                          ) : null}
                          {params.InputProps.endAdornment}
                        </>
                      ),
                    }}
                  />
                )}
              />
            </Stack>
          </Box>
          <Box sx={{ mb: 5 }}>
            <Box sx={{ fontSize: "20px", fontWeight: 700, mb: 2 }}>Мелодия</Box>
            <PhotoUploader />
          </Box>
          <LoadingButton
            type="submit"
            variant="contained"
            loading={loading}
            sx={{ minWidth: "232px" }}
          >
            Загрузить
          </LoadingButton>
        </Grid>
      </Grid>
    </Box>
  );
}

export default SetMusic;
