import Header from "components/Header";
import { Loader } from "components/Loader";
import React, { Suspense } from "react";
import { Routes, Route } from "react-router-dom";

const HomePage = React.lazy(() => import("pages/Home/Home"));
const LoginPage = React.lazy(() => import("pages/Authorization/Authorization"));
const SetMusicPage = React.lazy(() => import("pages/SetMusic/SetMusic"));

function MainRouter() {
  return (
    <Suspense fallback={<Loader />}>
      <Header />
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/set-music" element={<SetMusicPage />} />
        <Route path="/login" element={<LoginPage />} />
      </Routes>
    </Suspense>
  );
}

export default MainRouter;
