import { createApi } from "@reduxjs/toolkit/query/react";
import { baseQuery } from "utils/baseQuery";
import { ConfirmSmsDto, LoginDto, SendSmsResult } from "models/index";
import { injectToken } from "utils/injectToken";

export const appApi = createApi({
  reducerPath: "appApi",
  baseQuery,
  refetchOnFocus: true,
  endpoints: (build) => ({
    sendSms: build.mutation<SendSmsResult, LoginDto>({
      query: (body) => ({
        url: `/Auth/SendSms`,
        method: "POST",
        body,
      }),
    }),

    confirmSms: build.mutation<any, ConfirmSmsDto>({
      query: (body) => ({
        url: `/Auth/ConfirmSms`,
        method: "POST",
        headers: injectToken(),
        body,
      }),
    }),

    getSessionInfo: build.query<any, string>({
      query: (sessionId: string) => ({
        url: `/Auth/GetSessionInfo`,
        method: "POST",
        headers: injectToken(),
        params: {
          sessionId,
        },
      }),
    }),
  }),
});

export const {
  useSendSmsMutation,
  useConfirmSmsMutation,
  useGetSessionInfoQuery,
} = appApi;
