import { PayloadAction, createSlice } from "@reduxjs/toolkit";

interface AppState {
  sessionId: string | null;
  lang: string | null;
}

const initialState: AppState = {
  sessionId: localStorage.getItem("sessionId"),
  lang:
    localStorage.getItem("i18nextLng") || import.meta.env.VITE_DEFAULT_LANG!,
};

export const appSlice = createSlice({
  name: "app",
  initialState,
  reducers: {
    setSessionId(state, action: PayloadAction<string>) {
      localStorage.setItem("sessionId", action.payload);
      state.sessionId = action.payload;
    },
    removeSessionId(state) {
      localStorage.removeItem("sessionId");
      state.sessionId = null;
    },
    setLang(state, action: PayloadAction<string>) {
      localStorage.setItem("i18nextLng", action.payload);
      state.lang = action.payload;
    },
  },
});

export const appActions = appSlice.actions;
export const appReducer = appSlice.reducer;
