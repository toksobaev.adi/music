import { createApi } from "@reduxjs/toolkit/query/react";
import { baseQuery } from "utils/baseQuery";
import { CategoriesResponse } from "models/index";

export const categoriesApi = createApi({
  reducerPath: "categoriesApi",
  baseQuery,
  refetchOnFocus: true,
  endpoints: (build) => ({
    getCategories: build.query<CategoriesResponse, string>({
      query: () => ({
        url: `/Categories/GetCategories`,
        method: "GET",
      }),
    }),
  }),
});

export const { useGetCategoriesQuery } = categoriesApi;
