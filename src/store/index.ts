import { configureStore } from "@reduxjs/toolkit";
import { appApi } from "./app/app.api";
import { setupListeners } from "@reduxjs/toolkit/query";
import { appReducer } from "./app/app.slice";
import { musicApi } from "./music/music.api";
import { categoriesApi } from "./categories/categories.api";

export const store = configureStore({
  reducer: {
    [appApi.reducerPath]: appApi.reducer,
    [musicApi.reducerPath]: musicApi.reducer,
    [categoriesApi.reducerPath]: categoriesApi.reducer,
    app: appReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(
      appApi.middleware,
      musicApi.middleware,
      categoriesApi.middleware
    ),
});

setupListeners(store.dispatch);

export type RootState = ReturnType<typeof store.getState>;
