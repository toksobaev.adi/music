import { createApi } from "@reduxjs/toolkit/query/react";
import { baseQuery } from "utils/baseQuery";
import { MusicParams } from "models/index";

export const musicApi = createApi({
  reducerPath: "musicApi",
  baseQuery,
  refetchOnFocus: true,
  endpoints: (build) => ({
    getMusics: build.query<any, MusicParams>({
      query: (params: MusicParams) => ({
        url: `/Music/GetMusics`,
        method: "GET",
        params,
      }),
    }),
  }),
});

export const { useGetMusicsQuery } = musicApi;
