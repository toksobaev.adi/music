import { fetchBaseQuery } from "@reduxjs/toolkit/dist/query";

export const initialHeaders = {
  "Access-Control-Allow-Origin": "*",
  Accept: "application/json",
};

export const baseQuery = fetchBaseQuery({
  baseUrl: `${import.meta.env.VITE_WEBAPI_URL}/api`,
  headers: initialHeaders,
});
