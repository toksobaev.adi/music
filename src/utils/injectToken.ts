// We can use the following function to inject the JWT token through an interceptor

import { initialHeaders } from "./baseQuery";

// We get the `accessToken` from the localStorage that we set when we authenticate
export const injectToken = () => {
  try {
    const sessionId = localStorage.getItem("sessionId");
    if (sessionId != null) {
      return {
        ...initialHeaders,
        Authorization: sessionId,
      };
    }
  } catch (error: any) {
    throw new Error(error);
  }
};
