import { createTheme } from "@mui/material/styles";

const mainTheme = createTheme({
  palette: {
    primary: {
      main: "#4C12A1",
    },
  },
  typography: {
    fontFamily: "Golos Text, sans-serif",
  },
  components: {
    MuiFormControl: {
      styleOverrides: {
        root: {
          label: {
            lineHeight: "15px",
          },
        },
      },
    },
    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          borderRadius: "8px",
        },
        input: {
          height: "48px",
          boxSizing: "border-box",
          display: "flex",
          alignItems: "center",
        },
      },
    },
    MuiAutocomplete: {
      styleOverrides: {
        inputRoot: {
          height: "48px",
        },
        root: {
          label: {
            lineHeight: "28px",
          },
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          textTransform: "none",
          fontWeight: 400,
          fontSize: "16px",
          borderRadius: "8px",
          height: "48px",
          boxShadow: "none",
        },
      },
    },
    MuiTableContainer: {
      styleOverrides: {
        root: {
          borderRadius: "16px",
          boxShadow: "none",
        },
      },
    },
    MuiTableBody: {
      styleOverrides: {
        root: {
          tr: {
            "&:last-child": {
              "td, th": {
                border: "none",
              },
            },
          },
        },
      },
    },
  },
});

export default mainTheme;
