import { defineConfig, loadEnv } from "vite";
import react from "@vitejs/plugin-react";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: {
      pages: "/src/pages",
      components: "/src/components",
      hooks: "/src/hooks",
      img: "/src/assets/img",
      models: "/src/models",
      store: "/src/store",
      utils: "/src/utils",
    },
  },
});
